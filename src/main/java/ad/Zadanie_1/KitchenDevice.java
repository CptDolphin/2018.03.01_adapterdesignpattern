package ad.Zadanie_1;

public interface KitchenDevice {
    void turnDeviceOn();
    void turnDeviceOff();
}
