package ad.Zadanie_1.adapters;

import ad.Zadanie_1.KitchenDevice;
import ad.Zadanie_1.devices.Fridge;

public class FridgeMachineInheritAdapter extends Fridge implements KitchenDevice {

    public void turnDeviceOn() {
        super.switchOn();
    }

    public void turnDeviceOff() {
        super.switchOff();
    }
}
