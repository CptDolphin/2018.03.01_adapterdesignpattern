package ad.Zadanie_1.adapters;

import ad.Zadanie_1.KitchenDevice;
import ad.Zadanie_1.devices.Oven;

public class OvenMashineAdapter implements KitchenDevice {
    private Oven oven;

    public OvenMashineAdapter(Oven oven) {
        this.oven = oven;
    }

    public void turnDeviceOn() {
        oven.turnOn();
    }

    public void turnDeviceOff() {
        oven.turnOff();
    }
}
