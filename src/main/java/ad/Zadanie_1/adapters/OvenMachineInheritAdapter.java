package ad.Zadanie_1.adapters;

import ad.Zadanie_1.KitchenDevice;
import ad.Zadanie_1.devices.Oven;

public class OvenMachineInheritAdapter extends Oven implements KitchenDevice {
    public void turnDeviceOn() {
        super.turnOn();
    }

    public void turnDeviceOff() {
        super.turnOff();
    }
}
