package ad.Zadanie_1.adapters;

import ad.Zadanie_1.KitchenDevice;
import ad.Zadanie_1.devices.CoffeMachine;

public class CoffeMashineInheritAdapter extends CoffeMachine implements KitchenDevice {

    public void turnDeviceOn() {
        super.on();
    }

    public void turnDeviceOff() { super.off(); }
}
