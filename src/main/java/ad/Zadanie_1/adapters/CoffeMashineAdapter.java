package ad.Zadanie_1.adapters;

import ad.Zadanie_1.KitchenDevice;
import ad.Zadanie_1.devices.CoffeMachine;

public class CoffeMashineAdapter implements KitchenDevice {
    private CoffeMachine coffeMachine;

    public CoffeMashineAdapter(CoffeMachine coffeMachine) {
        this.coffeMachine = coffeMachine;
    }

    public void turnDeviceOn() {
        coffeMachine.on();
    }

    public void turnDeviceOff() {coffeMachine.off();}
}
