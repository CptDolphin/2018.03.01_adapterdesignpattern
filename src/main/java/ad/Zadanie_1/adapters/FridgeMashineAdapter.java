package ad.Zadanie_1.adapters;

import ad.Zadanie_1.KitchenDevice;
import ad.Zadanie_1.devices.Fridge;

public class FridgeMashineAdapter implements KitchenDevice {
    private Fridge fridge;

    public FridgeMashineAdapter(Fridge fridge) {
        this.fridge = fridge;
    }

    public void turnDeviceOn() {
        fridge.switchOn();
    }

    public void turnDeviceOff() {
        fridge.switchOff();
    }
}
