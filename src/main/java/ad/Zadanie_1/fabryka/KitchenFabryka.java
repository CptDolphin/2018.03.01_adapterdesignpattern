package ad.Zadanie_1.fabryka;

import ad.Zadanie_1.KitchenDevice;
import ad.Zadanie_1.adapters.*;

public class KitchenFabryka {

    public KitchenDevice getFabryka(String command) {
        if (command.equalsIgnoreCase("oven")) {
            return new OvenMachineInheritAdapter();
        } else if (command.equalsIgnoreCase("coffe")) {
            return new CoffeMashineInheritAdapter();
        } else if (command.equalsIgnoreCase("fridge")) {
            return new FridgeMachineInheritAdapter();
        }
        return null;
    }

//    public Oven getOven();
//
//    public CoffeMachine getCoffeMachine();
//
//    public Fridge getFridge();
}
