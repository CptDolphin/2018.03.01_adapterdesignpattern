package ad.Zadanie_1;

import ad.Zadanie_1.adapters.CoffeMashineAdapter;
import ad.Zadanie_1.adapters.FridgeMashineAdapter;
import ad.Zadanie_1.adapters.OvenMashineAdapter;
import ad.Zadanie_1.devices.CoffeMachine;
import ad.Zadanie_1.devices.Fridge;
import ad.Zadanie_1.devices.Oven;

import java.util.ArrayList;
import java.util.List;

public class Kitchen {
    private List<KitchenDevice> deviceList;

    public Kitchen() {
        this.deviceList = new ArrayList<KitchenDevice>();
        this.deviceList.add(new CoffeMashineAdapter(new CoffeMachine()));
        this.deviceList.add(new FridgeMashineAdapter(new Fridge()));
        this.deviceList.add(new OvenMashineAdapter((new Oven())));
    }

    public void turnEverythingOn(){
        for (KitchenDevice device : deviceList) {
            device.turnDeviceOn();
        }
    }

    public void turnEveryhingOff(){
        for(KitchenDevice device :deviceList){
            device.turnDeviceOff();
        }
    }
}
