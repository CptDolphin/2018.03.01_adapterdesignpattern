package ad.wstep.otherDevices;

import ad.wstep.Device;

public class WatchDeviceInheritAdapter extends WatchDevice implements Device{
    public boolean isOn() {
        return super.isTurnedOn();
    }

    public void turnOn() {
        super.setOnOff(true);
    }

    public void setParameter(String what, Object toWhat) {
        if(what.equals("reminder")){
            super.setReminder(String.valueOf(toWhat));
        }
    }
}
