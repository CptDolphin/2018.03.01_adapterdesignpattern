package ad.wstep.otherDevices;

import ad.wstep.Device;

public class WatchDeviceAdapter implements Device {
    private WatchDevice device;

    public WatchDeviceAdapter(WatchDevice device) {
        this.device = device;
    }

    public boolean isOn() {
        return device.isTurnedOn();
    }

    public void turnOn() {
        device.setOnOff(true);
    }

    public void setParameter(String what, Object toWhat) {
        if (what.equals("reminder")) {
            device.setReminder(String.valueOf(toWhat));
        }
    }
//
//    @Override
//    public int getBatteryLevel() {
//        return device.getBatteryLevel();
//    }
}
