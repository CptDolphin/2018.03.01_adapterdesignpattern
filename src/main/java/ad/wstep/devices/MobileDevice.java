package ad.wstep.devices;

import ad.wstep.Device;

public class MobileDevice implements Device{
    private String manufacturer;
    private boolean on;

    public boolean isOn() {
        return on;
    }

    public void turnOn() {
        on = true;
    }

    public void setParameter(String what, Object toWhat) {
        if (what.equals("manufacturer")) {
            this.manufacturer = String.valueOf(toWhat);
        }
    }
}
