package ad.wstep;

public interface Device {
    boolean isOn();
    void turnOn();
    void setParameter(String what, Object toWhat);

}
