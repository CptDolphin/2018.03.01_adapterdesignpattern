package ad.Zadanie_2.provider3;

import ad.Zadanie_2.PatiensAdapter;
import ad.Zadanie_2.PeopleRegistryDevice;
import ad.Zadanie_2.provider1.Person;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class StaticDataProviderAdapter implements PeopleRegistryDevice {
    StaticDataProvider staticDataProvider;

    public StaticDataProviderAdapter(StaticDataProvider staticDataProvider) {
        this.staticDataProvider = staticDataProvider;
    }

    @Override
    public List<PatiensAdapter> getUsers() {
        List<Human> humanList = staticDataProvider.getHumanList();
        return humanList.stream().map(human -> new PatiensAdapter(human)).collect(Collectors.toList());
    }
}
