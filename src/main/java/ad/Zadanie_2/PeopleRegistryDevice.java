package ad.Zadanie_2;

import java.util.List;

public interface PeopleRegistryDevice{
    List<PatiensAdapter> getUsers();
}
