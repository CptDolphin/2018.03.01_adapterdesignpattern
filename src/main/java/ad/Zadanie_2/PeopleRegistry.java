package ad.Zadanie_2;

import ad.Zadanie_2.provider1.FileDataProvider;
import ad.Zadanie_2.provider1.FileDataProviderAdapter;
import ad.Zadanie_2.provider2.RandomDataProvider;
import ad.Zadanie_2.provider2.RandomDataProviderAdapter;
import ad.Zadanie_2.provider3.StaticDataProvider;
import ad.Zadanie_2.provider3.StaticDataProviderAdapter;

import java.util.ArrayList;
import java.util.List;

public class PeopleRegistry {
    private List<PeopleRegistryDevice> registryList;

    public PeopleRegistry() {
        this.registryList = new ArrayList<>();
        registryList.add(new RandomDataProviderAdapter(new RandomDataProvider()));
        registryList.add(new FileDataProviderAdapter(new FileDataProvider()));
        registryList.add(new StaticDataProviderAdapter(new StaticDataProvider()));
    }


    public void print(){
        for (PeopleRegistryDevice p:registryList){
            List<PatiensAdapter> list = p.getUsers();
            System.out.println(list.toString());
        }
    }
}