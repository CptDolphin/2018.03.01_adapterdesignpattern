package ad.Zadanie_2;

import ad.Zadanie_2.provider1.Person;
import ad.Zadanie_2.provider2.User;
import ad.Zadanie_2.provider3.Human;

import java.time.LocalDate;

public class PatiensAdapter{
    private int id;
    private String name;
    private String surname;
    private String pesel;
    private LocalDate birthDate;

    public PatiensAdapter(Human human) {
        this.id = human.getId();
        this.name = human.getName();
        this.surname = human.getSurname();
    }

    public PatiensAdapter(Person person) {
        this.id = person.getId();
        this.name = person.getName();
        this.surname = person.getSurname();
        this.pesel = person.getPesel();
        this.birthDate = person.getBirthDate();
    }

    public PatiensAdapter(User user) {
        this.id = user.getIdentifier();
//        this.name = user.getNameAndSurname().split("-")[0];
//        this.surname  = user.getNameAndSurname().split("-")[1];
        this.pesel = user.getSocialIdentifier();
        this.birthDate = user.getBirthDate();
    }

    @Override
    public String toString() {
        return "PatiensAdapter{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", pesel='" + pesel + '\'' +
                ", birthDate=" + birthDate +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }
}
