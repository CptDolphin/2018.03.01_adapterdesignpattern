package ad.Zadanie_2.provider1;

import ad.Zadanie_2.PatiensAdapter;
import ad.Zadanie_2.PeopleRegistryDevice;

import java.util.List;
import java.util.stream.Collectors;

public class FileDataProviderAdapter implements PeopleRegistryDevice {
    FileDataProvider fileDataProvider;

    public FileDataProviderAdapter(FileDataProvider fileDataProvider) {
        this.fileDataProvider = fileDataProvider;
        fileDataProvider.initAndLoad();
    }

    @Override
    public List<PatiensAdapter> getUsers() {
        List<Person> personList = fileDataProvider.getPeopleList();
        return personList.stream().map(person -> new PatiensAdapter(person)).collect(Collectors.toList());
    }
}
