package ad.Zadanie_2.provider2;

import ad.Zadanie_2.PatiensAdapter;
import ad.Zadanie_2.PeopleRegistryDevice;

import java.util.List;
import java.util.stream.Collectors;

public class RandomDataProviderAdapter implements PeopleRegistryDevice {
    RandomDataProvider randomDataProvider;

    public RandomDataProviderAdapter(RandomDataProvider randomDataProvider) {
        this.randomDataProvider = randomDataProvider;
    }

    @Override
    public List<PatiensAdapter> getUsers() {
        List<User> userList = randomDataProvider.getSystemUsersList();
        return userList.stream().map(user -> new PatiensAdapter(user)).collect(Collectors.toList());
    }
}
